import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class bcd {

	public static void main(String[] args) {
		JSONObject employeeDetails = new JSONObject();
		JSONArray employeeList = new JSONArray();
		for(int i=0;i<5;i++) {
		
		employeeDetails.put("firstName", "Lokesh");
		employeeDetails.put("lastName", "Gupta");
		employeeDetails.put("website", "howtodoinjava.com");
		
		employeeList.add(employeeDetails);
		}

//		JSONObject employeeObject = new JSONObject();
//		employeeObject.put("employee", employeeDetails);

		// Second Employee
//		JSONObject employeeDetails2 = new JSONObject();
//		employeeDetails2.put("firstName", "Brian");
//		employeeDetails2.put("lastName", "Schultz");
//		employeeDetails2.put("website", "example.com");

//		JSONObject employeeObject2 = new JSONObject();
//		employeeObject2.put("employee", employeeDetails2);

		// Add employees to list
//		JSONArray employeeList = new JSONArray();
//		employeeList.add(employeeDetails);
//		employeeList.add(employeeDetails2);

		// Write JSON file
		try (FileWriter file = new FileWriter("employees.json")) {
			// We can write any JSONArray or JSONObject instance to the file
			file.write(employeeList.toJSONString());
			file.flush();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
