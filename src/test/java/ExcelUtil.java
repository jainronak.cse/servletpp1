import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtil {

	public void createTab() throws Exception {
		Workbook wb = new XSSFWorkbook();
		// An output stream accepts output bytes and sends them to sink.
		OutputStream fileOut = new FileOutputStream(new File("TestData.xls"));
		// Creating Sheets using sheet object
		Sheet sheet1 = wb.createSheet("Arial1");
		Sheet sheet2 = wb.createSheet("Arial2");
		Sheet sheet3 = wb.createSheet("Arial3");
		System.out.println("Sheets Has been Created successfully");
		wb.write(fileOut);
		wb.close();
	}

	public static void writeData(String filePath,String shtName,int rowNum,int colNum,String colData) throws Exception {
		// Declare a variable 'filePath' with data type String to store the path of the
		// file.
		
		File file = new File(filePath);
		FileInputStream fis = new FileInputStream(file);
		XSSFWorkbook wb = new XSSFWorkbook(fis);
		XSSFSheet sheet = wb.getSheet(shtName);
		XSSFRow R=sheet.getRow(0);
		XSSFRow row=sheet.getRow(rowNum-1);
		sheet.autoSizeColumn(colNum);
		row = sheet.createRow(0); // Return type of getRow method is a XSSFRow.
		XSSFCell cell = row.createCell(colNum);
		cell.setCellValue(colData); // This method returns nothing.
		
		FileOutputStream fos = new FileOutputStream(filePath);
		wb.write(fos);
		fis.close();
		fos.close();
		System.out.println("Result Written Successfully");

	}

	public static void main(String[] args) throws Exception {
		// Creating Workbook instances
		ExcelUtil.writeData("C:\\Users\\dell\\NewEclipse\\DataExtract\\Data.xlsx","Sheet1",2,2,"rounak");
	}
}
